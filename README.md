**kraken-menu**

*Menu for Kraken Desktops , release 2018.10.02*

# PACKAGING

* Install package :

```sh
sudo apt install build-essential devscripts debhelper fakeroot
```

* Build .deb :

```sh
git clone https://gitlab.com/kraken-sec/pkg-deb/kraken-menu.git
mkdir kraken-menu/kraken-menu-2018.10.02
cp -r kraken-menu/debian/ kraken-menu/kraken-menu-2018.10.02/
cd kraken-menu/kraken-menu-2018.10.02/
```

```sh
 $ debuild -i -uc -us --build=all
```

